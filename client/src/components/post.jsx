export default function Post(){
    return(
        <div className="post">
        <div className="image">
          <img src="https://techcrunch.com/wp-content/uploads/2024/02/critical-infrastructure.jpg?w=730&crop=1" alt="" />
        </div>
        <div className="texts">
          <h2>China-backed Volt Typhoon hackers have lurked inside US critical infrastructure for ‘at least five years’</h2>
          <p className="info">
            <a className="author">Akbar Hadiq</a>
            <time>2024-02-05 16:12</time>
          </p>
          <p className='summary'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident repudiandae animi harum, quaerat qui iusto vel ullam illum aperiam nisi magni maxime dignissimos nostrum fugit necessitatibus labore repellat, minus suscipit.
          Maiores amet ad ipsam, quam illo impedit cum. Assumenda repellat quod hic quia. Eveniet excepturi quas distinctio voluptate maiores ullam, fuga facilis alias odio nobis at fugiat! Hic, dolor laborum.</p>
        </div>
      </div>
    )
}