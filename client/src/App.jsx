import './App.css';
// import Post from './components/post';
// import Header from './components/header';
import Layout from './components/layout';
import {Routes, Route} from 'react-router-dom'
import IndexPage from './pages/indexpage';
import LoginPage from './pages/login';
import RegisterPage from './pages/register';

function App() {
  return (
    <main>
      <Routes>
        {/* pake outlet jadi bisa nested route kenapa? biar navbar yang ada di Layout selalu kebawa bawa kemana mana*/}
        <Route path='/' element={<Layout/>}>
            <Route index element={
              <IndexPage></IndexPage>
            }>
            </Route>
            <Route path='/login' element={<LoginPage/>}></Route>
            <Route path='/register' element={<RegisterPage/>}></Route>
        </Route>
       
        
      </Routes>
      
    </main>
  );
}

export default App;
